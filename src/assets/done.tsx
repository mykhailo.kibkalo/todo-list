import * as React from "react";

export const DoneIcon = ({ ...props }: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill={props.fill || "#000000"}
      height={props.height || 28}
      width={props.width || 28}
      viewBox="0 -960 960 960"
    >
      <path d="M378-246 154-470l43-43 181 181 384-384 43 43-427 427Z" />
    </svg>
  );
};
