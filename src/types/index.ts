export enum ItemEnum {
  inProgress = "inProgress",
  done = "done",
  toDo = "toDo",
}

export type TodoItemType = {
  id: string;
  text: string;
  state: ItemEnum;
};
