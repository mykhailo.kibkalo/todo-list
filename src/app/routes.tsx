import { Route, Routes } from "react-router-dom";
import HomeView from "./views/HomeView";
import NotFoundView from "./views/NotFoundView";
import AboutView from "./views/AboutView";

export const RouteMap = () => (
  <Routes>
    <Route path="/" element={<HomeView />} />
    <Route path="/about" element={<AboutView />} />
    <Route element={<NotFoundView />} />
  </Routes>
);
