import React, { useEffect, useState } from "react";
import { ItemEnum, TodoItemType } from "../../types";
import TodoList from "../../components/ToDo/todoList";
import TodoCreate from "../../components/ToDo/todoCreate";
import BodyWrap from "../../components/BodyWrap";
import guid from "../../utils/guid";

const HomeView = () => {
  const [todoList, setTodoList] = useState<TodoItemType[]>([]);

  // update locale storage
  useEffect(() => {
    console.log("todoList is updated");
    if (todoList.length) {
      localStorage.setItem("todoItems", JSON.stringify(todoList));
    }
  }, [todoList]);

  // get locale storage
  useEffect(() => {
    const items = JSON.parse(localStorage.getItem("todoItems"));
    if (items) {
      setTodoList(items);
    }
  }, []);

  // create todos
  const createTodo = (text: string) => {
    const newTodo = {
      id: guid(),
      text: text,
      state: ItemEnum.toDo,
    };
    const prevState = [...todoList];
    prevState.push(newTodo);
    handleSort(prevState);
  };

  // update todos
  const updateTodo = (id: string, state: ItemEnum) => {
    const prevState = [...todoList];
    const indexForUpdate = prevState.findIndex((el) => el.id === id);
    console.log(indexForUpdate);
    prevState[indexForUpdate].state = state;
    handleSort(prevState);
  };

  // delete todos
  const deleteTodo = (id: string) => {
    const prevState = [...todoList];
    const newState = prevState.filter((el) => el.id !== id);
    handleSort(newState);
  };

  // sort todos
  const handleSort = (items: TodoItemType[]) => {
    const inProgressItems = items.filter(
      (el) => el.state === ItemEnum.inProgress,
    );
    const todoItems = items.filter((el) => el.state === ItemEnum.toDo);
    const doneItems = items.filter((el) => el.state === ItemEnum.done);
    const sortedItems = inProgressItems.concat(todoItems).concat(doneItems);
    setTodoList(sortedItems);
  };

  return (
    <BodyWrap>
      <TodoCreate createTodo={createTodo} />
      <TodoList
        updateTodo={updateTodo}
        deleteTodo={deleteTodo}
        items={todoList}
      />
    </BodyWrap>
  );
};

export default HomeView;
