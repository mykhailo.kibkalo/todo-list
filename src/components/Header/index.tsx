import React from "react";
import "./styles.css";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <div className="header">
      <Link to="/about">About</Link>
      <Link to="/">Home</Link>
    </div>
  );
};

export default Header;
