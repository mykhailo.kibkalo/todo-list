import React from "react";
import { ItemEnum, TodoItemType } from "../../../types";
import TodoItem from "../todoItem";

type TodoListProps = {
  items: TodoItemType[];
  deleteTodo: (id: string) => void;
  updateTodo: (id: string, state: ItemEnum) => void;
};

const TodoList = (props: TodoListProps) => {
  const { items, deleteTodo, updateTodo } = props;

  const inProgressItems = items.filter(
    (el) => el.state === ItemEnum.inProgress,
  );
  const todoItems = items.filter((el) => el.state === ItemEnum.toDo);
  const doneItems = items.filter((el) => el.state === ItemEnum.done);

  return (
    <div className="list">
      {inProgressItems && inProgressItems.length ? (
        <>
          <div> In Progress To Do:</div>
          {inProgressItems.map((el) => (
            <div id={el.id}>
              <TodoItem
                updateTodo={updateTodo}
                deleteTodo={deleteTodo}
                item={el}
              />
            </div>
          ))}
        </>
      ) : (
        <></>
      )}

      {todoItems && todoItems.length ? (
        <>
          <div> To Do:</div>
          {todoItems.map((el) => (
            <div id={el.id}>
              <TodoItem
                updateTodo={updateTodo}
                deleteTodo={deleteTodo}
                item={el}
              />
            </div>
          ))}
        </>
      ) : (
        <></>
      )}

      {doneItems && doneItems.length ? (
        <>
          <div> Done:</div>
          {doneItems.map((el) => (
            <div id={el.id}>
              <TodoItem
                updateTodo={updateTodo}
                deleteTodo={deleteTodo}
                item={el}
              />
            </div>
          ))}
        </>
      ) : (
        <></>
      )}
    </div>
  );
};

export default TodoList;
