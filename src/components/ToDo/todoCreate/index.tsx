import React, { useState } from "react";
import "./styles.css";

type TodoCreateProps = {
  createTodo: (text: string) => void;
};

const TodoCreate = (props: TodoCreateProps) => {
  const { createTodo } = props;
  const [todoText, setTodoText] = useState("");

  return (
    <div className="form__group field">
      <input
        type="input"
        className="form__field"
        placeholder="ToDo name"
        name="name"
        id="newToDo"
        value={todoText}
        onChange={(e) => setTodoText(e.target.value)}
        required
      />
      <label htmlFor="newToDo" className="form__label">
        New ToDo
      </label>

      <div
        onClick={() => {
          createTodo(todoText);
          setTodoText("");
        }}
        className="btn"
      >
        Add ToDo
      </div>
    </div>
  );
};

export default TodoCreate;
