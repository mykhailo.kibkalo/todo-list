import React from "react";
import "./styles.css";
import CheckBox from "../checkBox";
import { ItemEnum, TodoItemType } from "../../../types";
import { DeleteIcon } from "../../../assets/delete";

type TodoItemProps = {
  item: TodoItemType;
  deleteTodo: (id: string) => void;
  updateTodo: (id: string, state: ItemEnum) => void;
};

const TodoItem = (props: TodoItemProps) => {
  const { item, deleteTodo, updateTodo } = props;

  const handleCheckBoxClick = () => {
    if (item.state === ItemEnum.toDo) {
      updateTodo(item.id, ItemEnum.inProgress);
    } else if (item.state === ItemEnum.inProgress) {
      updateTodo(item.id, ItemEnum.done);
    }
  };

  return (
    <div className="item-wrap">
      <CheckBox onClick={handleCheckBoxClick} type={item.state} />
      <div className={item.state === ItemEnum.done ? "strike" : ""}>
        {item.text}
      </div>
      <DeleteIcon onClick={() => deleteTodo(item.id)} fill="red" />
    </div>
  );
};

export default TodoItem;
