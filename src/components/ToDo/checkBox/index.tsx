import React from "react";
import "./styles.css";
import { DoneIcon } from "../../../assets/done";
import { LoadIcon } from "../../../assets/load";
import { ItemEnum } from "../../../types";

type CheckBoxProps = {
  type: ItemEnum;
  onClick: () => void;
};

const CheckBox = (props: CheckBoxProps) => {
  const { type, onClick } = props;

  const getIcon = () => {
    if (type === ItemEnum.toDo) {
      return <></>;
    } else if (type === ItemEnum.inProgress) {
      return (
        <div className="load-icon">
          <LoadIcon />
        </div>
      );
    } else {
      return <DoneIcon />;
    }
  };

  return (
    <div
      onClick={onClick}
      className="checkBoxWrap"
      style={{ cursor: type !== ItemEnum.done ? "pointer" : "default" }}
    >
      {getIcon()}
    </div>
  );
};

export default CheckBox;
