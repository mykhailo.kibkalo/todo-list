import React, { ReactNode } from "react";
import "./styles.css";

type MainWrapProps = {
  children: ReactNode;
};

const MainWrap = (props: MainWrapProps) => {
  return <div className="mainWrap">{props.children}</div>;
};

export default MainWrap;
