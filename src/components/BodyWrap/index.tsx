import React, { ReactNode } from "react";
import "./styles.css";

type BodyWrapProps = {
  children: ReactNode;
};

const BodyWrap = (props: BodyWrapProps) => {
  return <div className="bodyWrap">{props.children}</div>;
};

export default BodyWrap;
