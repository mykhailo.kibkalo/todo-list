import React from "react";
import "./App.css";
import { RouteMap } from "./app/routes";
import { BrowserRouter } from "react-router-dom";
import Header from "./components/Header";
import Footer from "./components/Footer";
import MainWrap from "./components/MainWrap";

function App() {
  return (
    <BrowserRouter>
      <MainWrap>
        <Header />
        <RouteMap />
        <Footer />
      </MainWrap>
    </BrowserRouter>
  );
}

export default App;
